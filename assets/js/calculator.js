function Calculator(answers) {
    this.answers = answers;
    this.score = 0;
    this.ppm = 0;
};

Calculator.prototype.getAnswer = function(answerId) {
    var answer = '';
    this.answers.forEach(function(element) {
        if (element.id == answerId) { 
            answer = element; 
            return;
        }
    });

    return answer;
}

Calculator.prototype.calculate = function() {
    var self = this;

    this.answers.forEach(function(userAnswer, index) {
        switch(userAnswer.id) {
        case 'Question1':
            var ppmScore = self.calculatePpmScore(userAnswer.value);
            self.score += (ppmScore * 0.12);
            break;
        case 'Question6':
            var questionFive = self.getAnswer("Question5");
            var questionFiveOptions = { normal: 1, hard: 2, veryHard: 3 } 
            var point = 0;
            if (userAnswer.value <= 3) {
		point = 1
            }
	    else if(userAnswer.value > 3 && userAnswer.value <= 5) {
		point = 2
            }
	    else
		point = 3
            self.score += point
            break;
        case 'Question7':
            if(userAnswer.value == 2) { self.score += 3; }
            else if(userAnswer.value == 3) { self.score += 2; }
	    else
		self.score +=1;
            break;
	case 'Question8':
            if(userAnswer.value == 2) { self.score += 10; }
            break;

        case 'Question13':
            var point = 0;
            
            if(userAnswer.value == 2)
		self.score += 2;
            break;
        case 'Question14':
            if(userAnswer.value == 1) { self.score += 6; }
            break;
        case 'Question15':
            var point = 0;
            if(userAnswer.value == 2) { point = 6; }
            else if(userAnswer.value == 3) { point = 9; }
            
            self.score += point
            break;
        default:
            break;
        }
    });

    return this.score;
};

Calculator.prototype.calculatePpmScore = function(cityOrRegion) {
    this.ppm = this.calculatePpm(cityOrRegion);
    
    if (this.ppm <= 100) {
        return 1;
    } else if( this.ppm >= 101 && this.ppm <= 150 ) {
        return 2;
    } else if( this.ppm >= 151 && this.ppm <= 250 ) {
        return 3;
    } else {
        return 3;
    }
}

Calculator.prototype.calculatePpm = function(cityOrRegion) {

    var score = 0;

    var region = {
        MARMARA: { key: "marmara", average: 173 },
        EGE: { key: "ege", average: 231 },
        AKDENIZ: { key: "akdeniz", average: 247 },
        KARADENIZ: { key: "karadeniz", average: 212 },
        IC_ANADOLU: { key: "ic-anadolu", average: 212 },
        DOGU_GUNEYDOGU: { key: "dogu-guneydogu", average: 180 },
    };

    var ppmData = [
        { id: region.MARMARA.key , value: region.MARMARA.average },
        { id: region.EGE.key , value: region.EGE.average },
        { id: region.AKDENIZ.key , value: region.AKDENIZ.average },
        { id: region.KARADENIZ.key , value: region.KARADENIZ.average },
        { id: region.IC_ANADOLU.key , value: region.IC_ANADOLU.average },
        { id: region.DOGU_GUNEYDOGU.key , value: region.DOGU_GUNEYDOGU.average },
        { id: 34 , value: 150 },
        { id: 41 , value: 107 },
        { id: 59 , value: 166 },
        { id: 54 , value: 125 },
        { id: 22 , value: 268 },
        { id: 10 , value: 171 },
        { id: 16 , value: 222 },
        { id: 20 , value: 286 },
        { id: 35 , value: 243 },
        { id: 48 , value: 247 },
        { id: 32 , value: 190 },
        { id: 45 , value: 294 },
        { id: 9 , value: 161 },
        { id: 3 , value: 197 },
        { id: 31 , value: 229 },
        { id: 33 , value: 214 },
        { id: 7 , value: 329 },
        { id: 46 , value: 208 },
        { id: 1 , value: 256 },
        { id: 67 , value: 132 },
        { id: 14 , value: 377 },
        { id: 61 , value: 181 },
        { id: 55 , value: 159 },
        { id: 6 , value: 120 },
        { id: 42 , value: 322 },
        { id: 19 , value: 296 },
        { id: 58 , value: 205 },
        { id: 68 , value: 252 },
        { id: 26 , value: 280 },
        { id: 38 , value: 122 },
        { id: 60 , value: 390 },
        { id: 63 , value: 164 },
        { id: 23 , value: 134 },
        { id: 24 , value: 208 },
        { id: 47 , value: 263 },
        { id: 27 , value: 255 },
        { id: 24 , value: 76 },
        { id: 44 , value: 162 },
        { id: 65 , value: 250 },
        { id: 21 , value: 136 },
        { id: 36 , value: 155 },
    ];

    ppmData.forEach(function(row) {
        if(row.id == cityOrRegion) {
            score = row.value;
            return score;
        }
    });

    return score;
}
