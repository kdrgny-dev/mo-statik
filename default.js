
var domObject = {
    welcomePage: "#WelcomePage",
    btnStart: "#WelcomePage a.start",
    btnNextVideo: "#VideoWelcome .next-video",
    videoPage: "#VideoPage",
    questionWrapper: "#Page .question-wrapper",
    btnNext: "a.btn-next",
    selectbox: "#Page .question-wrapper .options.selectbox select",
    btnAnswer: "#Page .question-wrapper .options.button a",
    activeClass: "active",
    videoWrapperClass: "video-wrapper",
    resultPage: "#ResultPage",
    resultTitle: "#ResultPage .text-wrapper h1",
    resultDescription: "#ResultPage .text-wrapper p",
    formPage: "#FormPage",
    btnGoToForm: "#ResultPage a.goToForm",
    formResultPage: "#FormResultPage",
    form: "#form",
    btnSubmitForm: "#submitForm"
};

var IS_MOBILE = /Android|webOS|iPhone|iPad|iPod|pocket|psp|kindle|avantgo|blazer|midori|Tablet|Palm|maemo|plucker|phone|BlackBerry|symbian|IEMobile|mobile|ZuneWP7|Windows Phone|Opera Mini/i.test(navigator.userAgent);
var PLATFORM = { WEB: "web", MOBILE: "mobile" };
var TRACKING = { campaign: "makinemin-omru-net" };
var videoEnded;

var getUserAnswers = function() {
    var answers = [];

    $(domObject.questionWrapper).each(function(index, element) {
        var questionId = $(element).attr("id");
        answers.push({
            id: questionId, 
            value: $(element).find("input[type='hidden']").val()
        });
    });
    
    return answers;
};

var getQuestionAnswer = function(questionId) {
    var answer = '';
    getUserAnswers().forEach(function(element) {
        if (element.id == questionId) { 
            answer = element; 
            return;
        }
    });

    return answer;
}

var calculateAndOpenResultPage = function() {
    var calculator = new Calculator(getUserAnswers());
    var score = calculator.calculate();

    var pageContent = getResultPageContent(score, calculator.ppm);
    $(domObject.resultTitle).html(pageContent.title);
    $(domObject.resultDescription).html(pageContent.description);
    $(domObject.resultPage).addClass(domObject.activeClass);
    delete(calculator);
};

var showAndPlayVideo = function(VideoWrapperDomObject) {
    var activeVideo = $("." + domObject.videoWrapperClass + ".active");
    activeVideo.find("video")[0].src = "";
    activeVideo.remove();
    VideoWrapperDomObject.addClass(domObject.activeClass);
    VideoWrapperDomObject.find("video")[0].load();
    VideoWrapperDomObject.find("video")[0].play();
    $(domObject.videoPage).fadeIn();
};

var getNextVideoId = function(questionId) {
    var answer  = getQuestionAnswer(questionId).value;
    var videoId = "";

    switch(questionId) {
        case "Question1":
            videoId = "VideoQ2";
            break;
        case "Question2":
            if(answer <= 2) { videoId = "VideoQ2R1"; }
            else if(answer <= 5) { videoId = "VideoQ2R2"; }
            else if(answer <= 7) { videoId = "VideoQ2R3"; }
            else if(answer <= 9) { videoId = "VideoQ2R4"; }
            else { videoId = "VideoQ2R5"; }
            break;
        case "Question3":
            videoId = "VideoQ5";
            break;
        case "Question5":
            if(answer == 1) { videoId = "VideoQ5R1"; }
            else if(answer == 2) { videoId = "VideoQ5R2"; }
            else if(answer == 3) { videoId = "VideoQ5R3"; }
            break;
        case "Question6":
            videoId = "VideoQ7";
            break;
        case "Question7":
            if(answer == 1) { videoId = "VideoQ7R1"; }
            else if(answer == 2) { videoId = "VideoQ7R2"; }
            break;
        case "Question8":
            if(answer == 1) { videoId = "VideoQ9"; }
            else if(answer == 2) { videoId = "VideoQ8R2"; }
            break;
        case "Question9":
            videoId = "VideoQ10";
            break;
        case "Question10":
            if(answer == 1) { videoId = "VideoQ10R1"; }
            else if(answer == 2) { videoId = "VideoQ10R2"; }
            else if(answer == 3) { videoId = "VideoQ10R3"; }
            break;
        case "Question11":
            if(answer == 1) { videoId = "VideoQ11R1"; }
            else { videoId = "VideoQ12"; }
            break;
        case "Question12":
            videoId = "VideoQ13";
            break;
        case "Question13":
            var questionTwelveAnswer  = getQuestionAnswer("Question12").value !== "" ? getQuestionAnswer("Question12").value : 1;
            if(questionTwelveAnswer == 1 && answer == 1) { videoId = "VideoQ13R1_1"; }
            else if(questionTwelveAnswer == 1 && answer == 2) { videoId = "VideoQ13R1_2"; }
            else if(questionTwelveAnswer == 2 && answer == 1) { videoId = "VideoQ13R2_1"; }
            else if(questionTwelveAnswer == 2 && answer == 2) { videoId = "VideoQ13R2_2"; }
            break;
        case "Question14":
            if(answer == 1) { videoId = "VideoQ14R1"; }
            else if(answer == 2) { videoId = "VideoQ14R2"; }
            break;
        case "Question15":
            if(answer == 1) { videoId = "VideoQ15R1"; }
            else if(answer == 2) { videoId = "VideoQ15R2"; }
            else if(answer == 3) { videoId = "VideoQ15R3"; }
            break;
    }
    return $("#" + videoId);
};

$( document ).ready(function() {

    // VISIBILITY FOR VIDEO
    $(function() {
        var removedVideos = IS_MOBILE ? $("video." + PLATFORM.WEB) : $("video." + PLATFORM.MOBILE);
        removedVideos.remove();
        delete(removedVideos);

        // Memory Leak Fix for Mobile Browser
        $.each( $("video"), function( key, video ) {
            $.each( video.getElementsByTagName("source"), function( key, source ) {
                var src = $(source).attr("data-src");
                if(typeof src !== "undefined") {
                    $(source).attr("src", $(source).attr("data-src"));
                }
            });
        });
    });

    // EVENT::CLICK (START BUTTON)
    $(domObject.btnStart).click(function() {
        $(domObject.welcomePage).removeClass(domObject.activeClass);
        showAndPlayVideo($("#VideoWelcome"));

        // ANALYTICS
        ga('create', 'UA-177219365-1');
        ga('send', 'event', "btn-start", 'click', TRACKING.campaign);

        setTimeout(function() {
            $(domObject.btnNextVideo).fadeIn();
        }, 5000);

        setTimeout(function() {
            $("#Question1").addClass(domObject.activeClass);
        }, 2000);
    });

    // EVENT::CLICK (NEXT WELCOME VIDEO)
    $(domObject.btnNextVideo).click(function() {
        $("#VideoWelcome video")[0].currentTime = 49.9;
        ga('send', 'event', "btn-skip-video", 'click', TRACKING.campaign);
    });

    // EVENT::VIDEO END
    videoEnded = function() {
        var wrapper = $(this).parent("." + domObject.videoWrapperClass);
        var nextVideoId = wrapper.attr("data-next-video-id");

        if( typeof nextVideoId !== undefined && nextVideoId !== "" ) {
            showAndPlayVideo($(nextVideoId));
            return;
        }

        $(domObject.videoPage).fadeOut();
    };
    $(".video-wrapper video").on('ended', videoEnded);

    // EVENT::CLICK
    $(domObject.btnNext).click(function() {
        var questionWrapper = $(this).parents(".question-wrapper");
        var questionId = questionWrapper.attr("id");
        var nextQuestionId = $(this).attr("data-next-question-id");
        $(this).closest(domObject.questionWrapper).removeClass(domObject.activeClass);

        try {
            var answer = questionWrapper.find('input[type="hidden"]')[0];
            var trackingTitle = $(answer).attr("name") + "_answer_" + $(answer).val();
            if(questionId === "Question1") {
                trackingTitle += "_" + $('select[name="cities"] option:selected')[0].text;
            }
            ga('send', 'event', trackingTitle, 'click', TRACKING.campaign);
        } catch(e) {
            console.log(e);
        }

        // SHOW VIDEO PAGE
        var nextVideoId = getNextVideoId(questionId);
        showAndPlayVideo($(nextVideoId));

        if(nextQuestionId === "#ResultPage") {
            setTimeout(function() {
                calculateAndOpenResultPage();
            }, 1000);
        } else {
            setTimeout(function() {
                $(nextQuestionId).addClass(domObject.activeClass);
            }, 1000);
        }
    });

    // EVENT::CHANGE
    $(domObject.selectbox).change(function() {
        var value = $(this).val().trim();
        var wrapper = $(this).parents(domObject.questionWrapper);
        wrapper.find("input[type='hidden']").val(value);
        value == "" ? wrapper.find(domObject.btnNext).removeClass(domObject.activeClass) : wrapper.find(domObject.btnNext).addClass(domObject.activeClass);
    });

    // EVENT::CLICK
    $(domObject.btnAnswer).click(function() {
        var value = $(this).attr("data-value").trim();
        var wrapper = $(this).parents(domObject.questionWrapper);
        var nextSceneId = $(this).attr("data-next-question-id");
        
        wrapper.find("input[type='hidden']").val(value);

        $(domObject.btnAnswer).removeClass(domObject.activeClass);
        $(this).addClass(domObject.activeClass);

        if(nextSceneId !== undefined) {
            wrapper.find(".btn-next").attr("data-next-question-id", nextSceneId);
        }

        value == "" ? wrapper.find(domObject.btnNext).removeClass(domObject.activeClass) : wrapper.find(domObject.btnNext).addClass(domObject.activeClass);
    });

    // EVENT::CLICK
    $(domObject.btnGoToForm).click(function() {
        $(domObject.resultPage).removeClass(domObject.activeClass);
        $(domObject.formPage).addClass(domObject.activeClass);
        ga('send', 'event', "btn-go-to-form", 'click', TRACKING.campaign);
    });

    // FORM SUBMIT
    $(domObject.form).submit(function(e) {
        e.preventDefault();

        var formData = {
            fullname: $.trim($("input[name='fullname']").val()),
            phone: $.trim($("input[name='phone']").val()),
            city: $.trim($("input[name='city']").val()),
            district: $.trim($("input[name='district']").val())
        }
        
        if(formData.fullname === "" || formData.phone === "" || formData.city === "" || formData.district === "") {
            alert("Servis randevusu oluşturabilmek için tüm alanları doldurmanız gerekiyor.");
            return false;
        }

        $(domObject.btnSubmitForm).fadeOut();

        ga('send', 'event', "form-submit", 'click', TRACKING.campaign);

        $.post( "service/save.php", formData).done(function(response) {
            $(domObject.formPage).removeClass(domObject.activeClass);
            $(domObject.formResultPage).addClass(domObject.activeClass);
        }).fail(function(xhr) {
            $(domObject.btnSubmitForm).fadeIn();
            if(xhr.status === 500) {
                alert("Hay aksi işleminiz yapılırken teknik bir sorun oluştu! Lütfen daha sonra yeniden deneyin.");
            } else if(xhr.status === 400) {
                alert("Lütfen tüm alanları doldurun.");
            }
        });

    });

    // SUBMIT BTN CLICK
    $(domObject.btnSubmitForm).click(function(e) {
        ga('send', 'event', "btn-form-submit", 'click', TRACKING.campaign);
        $(domObject.form).submit();
    });

    // DISCOUNT BTN CLICK
    $("a.discount").click(function(e) {
        ga('send', 'event', "btn-discount", 'click', TRACKING.campaign);
    });

    // SOCIAL BTN CLICK
    $(".social-media-wrapper a").click(function(e) {
        var socialMediaName = $(this).find("img").attr("alt");
        ga('send', 'event', "btn-" + socialMediaName, 'click', TRACKING.campaign);
    });

});

var getMetabolicAge = function(realAge, score, limit) {
    var metabolicAge;
    if( score < limit.YOUNG ) { metabolicAge = realAge; }
    else if( score < limit.MIDDLE ) { metabolicAge = Math.ceil(realAge + (realAge * 0.25)); }
    else if( score >= limit.OLD ) { metabolicAge = Math.ceil(realAge + (realAge * 0.50)); }
    return metabolicAge;
};

var setResultVideo = function(category, videoName) {
    try {
        var path = "/uploads/video/result/{category}/{platform}/{videoName}".replace("{platform}", IS_MOBILE ? "m" : "w");
        var videoSrc = path.replace("{category}", category);
        /*

        $.each( $("#VideoResult video")[0].getElementsByTagName("source"), function( key, element ) {
            var extension = ".mp4";
            if( element.type == "video/mp4" ) { extension = ".mp4" }
            else if( element.type == "video/ogg" ) { extension = ".ogg" }
            else if( element.type == "video/webm" ) { extension = ".webm" }
            $(element).attr("src", path.replace("{category}", category).replace("{videoName}", videoName + extension));
            $("#VideoResult video")[0].load();
        });*/

        /*var domVideo = $("#VideoResult video")[0];
        var videoSrc = path.replace("{category}", category);

        if (domVideo.canPlayType("video/mp4")) {
            videoSrc = videoSrc.replace("{videoName}", videoName + ".mp4");
        } else {
            videoSrc = videoSrc.replace("{videoName}", videoName + ".ogg");
        }
        domVideo.setAttribute("src",videoSrc);
        //domVideo.load();*/
        videoSrc = videoSrc.replace("{videoName}", videoName + ".mp4");
        $("#VideoResult").html('<video src="'+ videoSrc +'" playsinline></video>');
        $("#VideoResult video").on('ended', videoEnded);
    } catch(e) {
        console.log(e);
    }
};

var getResultPageContent = function(score, ppm) {
    var title = '';
    var description = 'Makinenizin<br>Bildiğiniz Yaşı: {REAL_AGE}<br>Gerçek Yaşı: {METABOLIC_AGE}';
    var limit = { YOUNG: 16.5, MIDDLE: 35, OLD: 35  };
    var category = { YOUNG: "young", MIDDLE: "middle", OLD: "old" };
    var realAge = parseInt(getQuestionAnswer("Question2").value);
    var metabolicAge = getMetabolicAge(parseInt(realAge), score, limit);
    var isChalky = ppm >= 101 ? true : false; // kirecli mi?
    
    var headlines = { YOUNG: 'BRAVOOO', MIDDLE: 'TAM ZAMANINDA YETİŞTİK', OLD: "YANİ NASIL SÖYLESEM BİLEMİYORUM" };
    description = description.replace("{REAL_AGE}", realAge).replace("{METABOLIC_AGE}", metabolicAge);

    if (realAge <= 2) {
        var videoName = "0-2";
        if( score < limit.YOUNG ) {
            title = headlines.YOUNG;
            setResultVideo(category.YOUNG, videoName);
        } else if( score < limit.MIDDLE ) {
            title = headlines.MIDDLE;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.MIDDLE, videoName);
        } else if( score >= limit.OLD ) {
            title = headlines.OLD;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.OLD, videoName);
        }
    } else if (realAge <= 5) {
        var videoName = "3-5";
        if( score < limit.YOUNG ) {
            title = headlines.YOUNG;
            setResultVideo(category.YOUNG, videoName);
        } else if( score < limit.MIDDLE ) {
            title = headlines.MIDDLE;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.MIDDLE, videoName);
        } else if( score >= limit.OLD ) {
            title = headlines.OLD;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.OLD, videoName);
        }
    } else if (realAge <= 7) {
        var videoName = "6-8";
        if( score < limit.YOUNG ) {
            title = headlines.YOUNG;
            setResultVideo(category.YOUNG, videoName);
        } else if( score < limit.MIDDLE ) {
            title = headlines.MIDDLE;
            setResultVideo(category.MIDDLE, videoName);
        } else if( score >= limit.OLD ) {
            title = headlines.OLD;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.OLD, videoName);
        }
    } else if (realAge <= 9) {
        var videoName = "9-10";
        if( score < limit.YOUNG ) {
            title = headlines.YOUNG;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.YOUNG, videoName);
        } else if( score < limit.MIDDLE ) {
            title = headlines.MIDDLE;
            setResultVideo(category.MIDDLE, videoName);
        } else if( score >= limit.OLD ) {
            title = headlines.OLD;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.OLD, videoName);
        }
    } else {
        var videoName = "10-plus";
        if( score < limit.YOUNG ) {
            title = headlines.YOUNG;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.YOUNG, videoName);
        } else if( score < limit.MIDDLE ) {
            title = headlines.MIDDLE;
            setResultVideo(category.MIDDLE, videoName);
        } else if( score >= limit.OLD ) {
            title = headlines.OLD;
            if( isChalky ) { videoName = videoName + "-chalky"; }
            setResultVideo(category.OLD, videoName);
        }
    }

    return { title: title, description: description };
};